///////////////////////////////////////////////////
//// Bellow this line, there may be dragons... ////
///////////////////////////////////////////////////

window.onload = () => {
    const canvas = document.getElementById("canvas");
    const ctx = canvas.getContext("2d");
    
    init().then(game => {
        window.onkeydown = event => onKeyDown(game, event);
        window.onkeyup = event => onKeyUp(game, event);
        canvas.onmousedown = event => onMouseDown(game, event);
        canvas.onmouseup = event => onMouseUp(game, event);
        canvas.onmousemove = event => onMouseMove(game, event);

        const getDeltaTime = makeGetDeltaTime();
        
        const mainLoop = () => {
            const deltaTime = getDeltaTime();
            update(game, deltaTime);
            draw(game, ctx);
            requestAnimationFrame(mainLoop);
        };
        requestAnimationFrame(mainLoop);
    }).catch(error => {
        alert(error);
    });
};

let loadImage = src => new Promise((resolve, reject) => {
    const img = new Image();
    img.onload = () => resolve(img);
    img.onerror = error => reject("can't load image: "+src);
    img.src = src;
});

let loadAudio = src => new Promise((resolve, reject) => {
    const audio = new Audio();
    audio.onload = () => resolve(audio);
    audio.addEventListener('canplaythrough', () => resolve(audio), false);
    audio.onerror = erorr => reject("can't load auido: "+src);
    audio.src = src;
});

let makeGetDeltaTime = () => {
    let oldDate = Date.now();
    return () => {
        const now = Date.now();
        const deltaTime = (now-oldDate) / 1000;
        oldDate = now;
        return deltaTime;
    };
};

let getMousePos = event => {
    const rect = event.target.getBoundingClientRect();
    return {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
};

/* you've been warned...

            <>=======()
           (/\___   /|\\          ()==========<>_
                 \_/ | \\        //|\   ______/ \)
                   \_|  \\      // | \_/
                     \|\/|\_   //  /\/
                      (oo)\ \_//  /
                     //_/\_\/ /  |
                    @@/  |=\  \  |
                         \_=\_ \ |
                           \==\ \|\_ snd
                        __(\===\(  )\
                       (((~) __(_/   |
                            (((~) \  /
                            ______/ /
                            '------'
*/
