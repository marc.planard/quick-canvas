/*
** this is an empty project, check out examples/game.js 
** to find tips to start
*/

const init = async () => {
    // here goes your init code.
    return null;
}

const update = (game, deltaTime) => {
    // here goes your update code.
};

const draw = (game, ctx) => {
    // here goes your drawing code
    ctx.fillStyle="#aaf";
    ctx.fillRect(0, 0, 600, 600);
    ctx.fillStyle="#fff";
    ctx.font = "42px Arial";
    const text = "no game yet";
    ctx.fillText(text, 300 - ctx.measureText(text).width/2, 300);
};

// bellow are your input handlers
const onKeyDown = (game, event) => {
};

const onKeyUp = (game, event) => {
};

const onMouseDown = (game, event) => {
};

const onMouseUp = (game, event) => {
};

const onMouseMove = (game, event) => {
};
