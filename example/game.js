/*
** These are quick and dirty bindings to the Canvas2d, Image and Audio APIs
** of HTML5 to make a game.
**
** There is just 3 functions to manage your whole game:
** init - update - draw
** and 5 functions to handle input events:
** onKeyUp - onKeyDown - onMouseUp - onMouseDown - onMouseMove
*/

/*
** the init function is called one time at the begining of the game.
** This is where you setup things and load resources.
**
** !! Be carefull with the async and await keywords, they are important !!
**
** await loadImage() returns an image which can then be drawn 
** (see draw function)
**
** await loadAudio() returns an audio object which can be played
** (see update function)
**
** init returns an object which will be passed to the other functions
** of your game.
*/

const init = async () => {
    const game = {
        width: 600,
        height: 600,
        background: await loadImage("data/background.jpg"),
        ball: { x: 200, y: 100,
                w: 25, h: 25,
                u: 2, v: -2,
                img: await loadImage("data/ball.png")
              },
        pad: { x: 250, y: 550,
               w: 100, h: 25,
               u: 0, v: 0,
               color: "#fff"
             },
    };
    game.objects = [ game.ball, game.pad ];
    game.ping_sound = await loadAudio("data/cymbaly-1.wav");
    game.fps = "-";
    return game;
}

/*
** the update function is called every new frame to update the state
** of the game. It takes the object returned by init as first argument
** and the time elapsed in seconds since the last frame in second
** argument.
*/
const update = (game, deltaTime) => {
    for(const obj of game.objects) {
        obj.x += obj.u * 100 * deltaTime;
        obj.y += obj.v * 100 * deltaTime;

        if (obj.x + obj.w > game.width) {
            obj.u *= -1;
            obj.x = game.width - obj.w;
            game.ping_sound.cloneNode().play();
        } else if (obj.x < 0 ) {
            obj.u *= -1;
            obj.x = 0;
            game.ping_sound.cloneNode().play();
        }
        
        if (obj.y < 0 ) {
            obj.v *= -1;
            obj.y = 0;
            game.ping_sound.cloneNode().play();
        } else if (obj.y + obj.h > game.height) {
            obj.v *= -1;
            obj.y = game.height - obj.h;
            game.ping_sound.cloneNode().play();
        }
    }
    game.fps = Math.round(1/deltaTime);
};

/*
** the draw function is called each time the display needs to be updated
** (typically every frame, after update). It takes the object returned
** by init as first argument and a canvas context as second argument,
** to draw the game on it.
**
** A reference for the canvas 2d API can be found here:
** https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D
*/
const draw = (game, ctx) => {
    ctx.drawImage(game.background, 0, 0, game.width, game.height);
    for(const obj of game.objects) {
        if (obj.img) {
            ctx.drawImage(obj.img, obj.x, obj.y, obj.w, obj.h);
        } else {
            ctx.fillStyle=obj.color;
            ctx.fillRect(obj.x,obj.y,obj.w,obj.h);
        }
    }
    ctx.fillStyle="#000";
    ctx.fillText(game.fps, 0, 10);
};

/*
** Called when a key on the keyboard is pressed
*/
const onKeyDown = (game, event) => {
    switch(event.key) {
    case "ArrowLeft":
        game.pad.u = -5;
        break;
    case "ArrowRight":
        game.pad.u = 5;
        break;
    default:
        console.log(event);
    }
};

/*
** Called when a key on the keyboard is released
*/
const onKeyUp = (game, event) => {
    switch(event.key) {
    case "ArrowLeft": 
        game.pad.u = 0;
        break;
    case "ArrowRight": 
        game.pad.u = 0;
        break;
    default:
        console.log(event);
    }
};

/*
** Called when a mouse button is pressed
*/
const onMouseDown = (game, event) => {
    const mouse_pos = getMousePos(event);
    const size = 10;
    
    game.objects.push({
        x: mouse_pos.x - size/2,
        y: mouse_pos.y - size/2,
        h: size,
        w: size,
        u: (0.5-Math.random())*10*Math.random(),
        v: (0.5-Math.random())*10*Math.random(),
        color: "#faa"
    });
};

/*
** Called when a mouse button is released
*/
const onMouseUp = (game, event) => {
    console.log(event);
};

/*
** Called each time the mouse cursor moves above the game canvas.
*/
const onMouseMove = (game, event) => {

};
